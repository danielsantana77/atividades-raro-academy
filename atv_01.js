"use strict";
const denisRichie = {
    name: "Denis Richie",
    birthData: new Date(1941, 9, 9),
    nacionality: "Norte Americano",
    bio: "was an American computer scientist.[1] He created the C programming language and, with long-time colleague Ken Thompson, the Unix operating system and B programming language.",
    occupationArea: [
        'linguagens de programação',
        'sistemas operacionais'
    ]
};
const graceHopper = {
    name: "Grace Hopper",
    birthData: new Date(1906, 12, 9),
    nacionality: "Estadunidense",
    bio: "foi uma matemática norte-americana. Hopper obteve o seu doutoramento em matemática pela Universidade de Yale em 1934. Foi uma das pioneiras no desenvolvimento do computador eletrónico e uma programadora de computadores (um dos primeiros programadores).",
    occupationArea: [
        'Matemática',
        'Linguagens de Programação'
    ]
};
const alanTuring = {
    name: "Alan Turing",
    birthData: new Date(1912, 6, 23),
    nacionality: "Inglês",
    bio: "foi um matemático britânico, pioneiro da computação e considerado o pai da ciência computacional e da inteligência artificial. Alan Mathison Turing (1912-1954), conhecido como Alan Turing, nasceu na cidade de Paddington, na Inglaterra, no dia 23 de junho de 1912.",
    occupationArea: [
        'Inteligência Artificial',
        'Matemático'
    ]
};
const johnVonNeumann = {
    name: "John von Neumann",
    birthData: new Date(1903, 12, 28),
    nacionality: "Hungaro",
    bio: "John Von Neumann foi um matemático brilhante e deixou às gerações futuras legados extremamente importantes. Nasceu a 28 de dezembro de 1903, em Budapeste, na Hungria, e foi um dos primeiros membros permanentes do Instituto de Estudos Avançados, na América do Norte, em 1930",
    occupationArea: [
        'Matemático'
    ]
};
console.log(denisRichie);
console.log('-------------------------------');
console.log(graceHopper);
console.log('-------------------------------');
console.log(alanTuring);
console.log('-------------------------------');
console.log(johnVonNeumann);
/* QUESTÃO 2*/
const cientistas = [];
cientistas.push(denisRichie);
cientistas.push(graceHopper);
cientistas.push(alanTuring);
cientistas.push(johnVonNeumann);
console.log(cientistas);
/* QUESTÃO 3*/
let valorMaior = 5;
let valorMenor = 10;
let aux = valorMaior;
valorMaior = valorMenor;
valorMenor = aux;
/*QUESTÃO 4*/
const nome = 'Daniel';
const idade = 25;
const saudacao = 'Olá, meu nome é #nome# e eu tenho #idade# anos.';
const newSaudacao = saudacao.replace('#nome#', nome);
const newSaudacao2 = newSaudacao.replace('#idade#', idade.toString());
console.log(newSaudacao2);
